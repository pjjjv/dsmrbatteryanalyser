# Battery Analyser
This Jupyter Notebook calculates the effect of a home battery on electrical energy delivered by and returned to the electrical grid. The initial purpose is to arrive at a cost reduction by kWh battery chart. This can then be used for investment analysis to calculate a pay back period.

## Prerequisites
This notebook uses the postgresql database of the [dsmr-reader](https://github.com/dsmrreader/dsmr-reader) project as data source, in particular digital meter energy and power measurements. It also uses site account data from the SolarEdge server, in particular solar production energy and power measurements. You can use any datasource provided you do small adaptations to the notebook.

## Installation
You'll need [Jupyter Notebooks](https://jupyter.org/). I would advise to install the notebooks by installing [Anaconda](https://www.anaconda.com/). Then you need to install several dependencies/packages. Below is a not limitative list (I'm  not sure what you need to install and what is installed by default):
- psycopg2
- dotenv 
- pandas 
- numpy
- python-dotenv
- joblib
- matplotlib

## Configuration
First you need to adapt the .env file so that you have the right credentials. Then you need to adapt several parameters in the notebook to suit your analysis. You can also leave some at the default values if you don't know the real value.

## Credits
Mathias Vanden Auweele - original (DsmrBatteryAnalyser.ipynb)

pjv - rewrite, add SolarEdge, add more documentation (BatteryAnalyzer.ipynb) 
